import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationRoutes } from './authentication.routing';
import { TranslateModule } from '@ngx-translate/core';

import { AppSettings } from '../app.settings';
// import { SharedModule } from '../app.module';
// export function provideConfig() {
//   return config;
// }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forChild(AuthenticationRoutes),
    TranslateModule.forChild(),
    // SharedModule
  ],
  providers: [
  ],
  declarations: [
    LoginComponent,
    SignupComponent,
  ]
})

export class AuthenticationModule {}
