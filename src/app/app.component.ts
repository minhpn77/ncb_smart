import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpModule } from '@angular/http';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(
        public router: Router,

    ) {
        this.redirectDashboard();
    }

    ngOnInit() {
    }

    redirectDashboard() {
        const isLoggedin = localStorage.getItem('isLoggedin');
        if (isLoggedin === 'true') {
            this.router.navigateByUrl('/dashboard');

        }

    }
}
