import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AuthService } from '../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Helper } from '../helper';



@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()],
    providers: [Helper]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;
    processLogin: any = 0;
    page_back: any = '';
    return_url: any = '';
    constructor(
        public router: Router,
        private formBuilder: FormBuilder,
        public authService: AuthService,
        public helper: Helper
    ) {}

    ngOnInit() {
        this.return_url    = this.helper.getParameterByName('return_url');
        this.page_back    = this.helper.getParameterByName('call-back');
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
          });
    }
    get Form() { return this.loginForm.controls; }

    keyDownFunction(event) {
        if (event.keyCode === 13) {
            this.onLoggedin();
        }
    }

    onLoggedin() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }

        // const formData = this.loginForm.value;
        const formData = 'username=' + this.loginForm.value.username + '&password=' + this.loginForm.value.password;
        console.log('000xx', formData);
        this.authService.login(formData)
            .then((result) => {

                this.onLoadAfterLogin(result);

        })
        .catch((err) => {
            this.processLogin = 0;
            if (err.status === 400) {
                this.processLogin = 0;
            }
        });
      }

    onLoadAfterLogin(data): void {
        localStorage.setItem('token', data.headers.get('Authorization'));
        localStorage.setItem('profile', JSON.stringify(data.json().body));
        if (this.page_back !== '' && this.page_back != null && this.page_back !== undefined) {
            this.router.navigateByUrl(this.page_back);
        } else {
            localStorage.setItem('isLoggedin', 'true');
            this.router.navigateByUrl('/dashboard');

        }
        //
        if (this.return_url !== '' && this.return_url != null && this.return_url !== undefined) {
            window.location.href = this.return_url;
        } else {
            localStorage.setItem('isLoggedin', 'true');
            this.router.navigateByUrl('/dashboard');
        }



    }
}
