import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'telecom-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  telecomNumberForm: FormGroup;
  submitted = false;
  telecomNumberId: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      this.telecomNumberId = parseInt(params.telecomNumberId);
    });
  }

  ngOnInit() {
    this.telecomNumberForm = this.formBuilder.group({
      telecom: ['', Validators.required],
      number_from: ['', Validators.required],
      number_to: ['', Validators.required],
    });
  }
  get Form() { return this.telecomNumberForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.telecomNumberForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.telecomNumberForm.value));
  }
  resetForm() {
    this.telecomNumberForm.reset();
  }
}









