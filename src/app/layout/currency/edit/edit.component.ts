import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';


@Component({
  selector: 'curency-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  currencyForm: FormGroup;
  submitted = false;
  currencyId: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      this.currencyId = parseInt(params.currencyId);
    });
  }

  ngOnInit() {
    this.currencyForm = this.formBuilder.group({
      code_currency: ['', Validators.required],
      name_currency: ['', Validators.required]

    });
  }
  get Form() { return this.currencyForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.currencyForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.currencyForm.value));
  }
  resetForm() {
    this.currencyForm.reset();
  }
}







