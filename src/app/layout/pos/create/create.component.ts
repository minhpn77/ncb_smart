import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'pos-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  posForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.posForm = this.formBuilder.group({
      posCode: ['', Validators.required],
      posName: ['', Validators.required]

    });
  }
  get Form() { return this.posForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.posForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.provinceForm.value));
  }
  resetForm() {
    this.posForm.reset();
  }
}


