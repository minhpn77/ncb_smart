import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

@Component({
  selector: 'pos-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  posForm: FormGroup;
  submitted = false;
  posId: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
  ) {
      this.route.params.subscribe(params => {
        this.posId = parseInt(params.posId);
    });
   }

  ngOnInit() {
    console.log('--provinceId', this.posId);
    this.posForm = this.formBuilder.group({
      posCode: ['', Validators.required],
      posName: ['', Validators.required]
    });
  }
  get Form() { return this.posForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.posForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.provinceForm.value));
  }
  resetForm() {
    this.posForm.reset();
  }
}




