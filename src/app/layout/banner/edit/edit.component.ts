import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'banner-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  bannerForm: FormGroup;
  submitted = false;
  Id: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      this.Id = parseInt(params.Id);
    });
  }

  ngOnInit() {
    this.bannerForm = this.formBuilder.group({
      nameBanner: ['', Validators.required],
      linkImage: ['', Validators.required],
      linkUrlVN: ['', Validators.required],
      linkUrlEN: ['', Validators.required]
    });
  }
  get Form() { return this.bannerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.bannerForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.provinceForm.value));
  }
  resetForm() {
    this.bannerForm.reset();
  }
}













