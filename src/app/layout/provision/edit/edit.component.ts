import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

@Component({
  selector: 'provision-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  provisionForm: FormGroup;
  submitted = false;
  provisionId: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
  ) {
      this.route.params.subscribe(params => {
        this.provisionId = parseInt(params.provisionId);
    });
   }

  ngOnInit() {
    console.log('--provinceId', this.provisionId);
    this.provisionForm = this.formBuilder.group({
      provisionName: ['', Validators.required],
      provisionLink: ['', Validators.required]
    });
  }
  get Form() { return this.provisionForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.provisionForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.provinceForm.value));
  }
  resetForm() {
    this.provisionForm.reset();
  }
}



