import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'pos-detail-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  posDetailForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.posDetailForm = this.formBuilder.group({
      address_pos_detail: ['', Validators.required],
      name_pos_detail: ['', Validators.required],
      location_address: ['', Validators.required],
      phone_number: ['', Validators.required],
      province: '',
      branch: '',
      district: '',
      pos: '',
      fax: '',
      contact: ''
    });
  }
  get Form() { return this.posDetailForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.posDetailForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.posDetailForm.value));
  }
  resetForm() {
    this.posDetailForm.reset();
  }
}


