import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'pos-detail-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  posDetailForm: FormGroup;
  submitted = false;
  posDetailId: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
        this.posDetailId = parseInt(params.posDetailId);
    });
  }

  ngOnInit() {
    this.posDetailForm = this.formBuilder.group({
      address_pos_detail: ['', Validators.required],
      name_pos_detail: ['', Validators.required],
      location_address: ['', Validators.required],
      phone_number: ['', Validators.required],
      province: '',
      branch: '',
      district: '',
      pos: '',
      fax: '',
      contact: ''
    });
  }
  get Form() { return this.posDetailForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.posDetailForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.posDetailForm.value));
  }
  resetForm() {
    this.posDetailForm.reset();
  }
}







