import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'province', loadChildren: './province/province.module#ProvinceModule' },
            { path: 'district', loadChildren: './district/district.module#DistrictModule' },
            { path: 'denominations', loadChildren: './denominations/denominations.module#DenominationsModule' },
            { path: 'bill-service', loadChildren: './bill-service/bill-service.module#BillServiceModule' },
            { path: 'provision', loadChildren: './provision/provision.module#ProvisionModule' },
            { path: 'branch', loadChildren: './branch/branch.module#BranchModule' },
            { path: 'user', loadChildren: './user/user.module#UserManagerModule' },
            { path: 'decen', loadChildren: './decen/decen.module#DecenModule' },
            { path: 'history', loadChildren: './history/history.module#HistoryModule' },
            { path: 'sales', loadChildren: './sales/sales.module#SalesModule' },
            { path: 'transaction-point', loadChildren: './pos-type/pos-type.module#PosTypeModule' },
            { path: 'transaction-room', loadChildren: './pos/pos.module#PosModule' },
            { path: 'transaction-more', loadChildren: './pos-detail/pos-detail.module#PosDetailModule' },
            { path: 'currency', loadChildren: './currency/currency.module#CurrencyModule' },
            { path: 'telecom', loadChildren: './telecom/telecom.module#TelecomModule' },
            { path: 'telecom-number', loadChildren: './telecom-number/telecom-number.module#TelecomNumberModule' },
            { path: 'configure', loadChildren: './configure/configure.module#ConfigureModule' },
            { path: 'promotion', loadChildren: './promotion/promotion.module#PromotionModule' },
            { path: 'service-mb', loadChildren: './service-mb/service-mb.module#ServiceMBModule' },
            { path: 'type-service', loadChildren: './type-service/type-service.module#TypeServiceMBModule' },
            { path: 'banner', loadChildren: './banner/banner.module#BannerModule' },
            { path: 'product', loadChildren: './product/product.module#ProductModule' },
            { path: 'service-limit', loadChildren: './service-limit/service-limit.module#ServiceLimitModule' },
            { path: 'charity', loadChildren: './charity/charity.module#CharityModule' },
            { path: 'user-session', loadChildren: './user-session/user-session.module#UserSessionModule' },
            { path: 'user-action-history', loadChildren: './user-action-history/user-action-history.module#UserActionHistoryModule' },
            { path: 'support', loadChildren: './support/support.module#SupportModule' },
            { path: 'feature-app', loadChildren: './feature/feature.module#FeatureAppModule' },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
