import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

@Component({
  selector: 'postype-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  posTypeForm: FormGroup;
  submitted = false;
  posTypeId: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
  ) {
      this.route.params.subscribe(params => {
        this.posTypeId = parseInt(params.posTypeId);
    });
   }

  ngOnInit() {
    console.log('--provinceId', this.posTypeId);
    this.posTypeForm = this.formBuilder.group({
      posTypeName: ['', Validators.required],
      posTypeCode: ['', Validators.required]
    });
  }
  get Form() { return this.posTypeForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.posTypeForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.provinceForm.value));
  }
  resetForm() {
    this.posTypeForm.reset();
  }
}




