import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { NCBService } from '../../../services/ncb.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [NCBService]
})
export class ListComponent implements OnInit {
  listPGD: any = [];
  listBranch: any = [];
  listRole: any = [];

  re_search: any = {
    fullName: '',
    // page: 0,
    // size: 10,
    transactionCode: '',
    branchCode: '',
    roleId: '',
    status: ''
  };

  search: any = {
    keyword: '',
    total_items: 0,
    page: 0,
    size: 10,
    previous_page: 0,
    process_load: 0,
    formDate: 0,
    toDate: 0,
  };
  listData: any = [];
  listPageSize: any = [10, 20, 30, 40, 50];
  listStatus: any = [
    {
      name: 'Tất cả',
      code: '',
    },
    {
      name: 'Active',
      code: 1,
    },
    {
      name: 'Inactive',
      code: 0,
    }
  ];
  constructor(
    private ncbService: NCBService,
    public toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.getListData(this.search);
    this.getListBranch();
    this.getListPGD();
    this.getListRole();
  }

  getListData(params) {
    this.search.process_load = 1;
    // xu ly
    this.listData = [];
    this.ncbService.getListUser(params).then((result) => {
      setTimeout(() => {
        this.search.process_load = 0;
        const body = result.json().body;
        this.listData = body.content;
        console.log('---xx', this.listData);
        this.search.total_items = body.totalElements;

      }, 500);
    }).catch((err) => {
      this.search.process_load = 0;
      this.toastr.error('Vui lòng thử lại', 'Lỗi hệ thống!');
    });

  }

  keyDownFunction(event) {

  }
  deleteItem(event, index) {
    Swal.fire({
      title: 'Bạn có chắc chắn xoá?',
      text: 'Dữ liệu đã xoá không thể khôi phục lại',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Không, trở lại'
    }).then((result) => {
      if (result.value) {
        this.listData.splice(index, 1);
        Swal.fire(
          'Đã xoá!',
          'Dữ liệu đã xoá hoàn toàn.',
          'success'
        );

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Huỷ bỏ',
          'Dữ liệu được bảo toàn :)',
          'error'
        );
      }
    });
  }
  cancelAction(event) {
    console.log('huy bo thanh con');

  }

  loadPage(page: any) {
    const page_number = page - 1;

    if (page_number !== this.search.previous_page) {
      this.search.page = page_number;
      this.search.previous_page = page_number;
      this.getListData(this.search);
    }
  }
  changeSize(size: number) {
    this.search.size = size;
    this.search.page = 0;

    this.getListData(this.search);

  }
  onSearch(params) {
    this.ncbService.searchUser(params).then((result) => {
      this.listData = [];
      setTimeout(() => {
        this.search.process_load = 0;
        this.listData = result.json().body;
        this.search.total_items = this.listData.length;

      }, 500);
    }).catch((err) => {
      this.search.process_load = 0;
      this.listData = [];
      this.toastr.error('Vui lòng thử lại', 'Lỗi hệ thống!');

    });
  }
  exportExcel() {

  }
  getListBranch() {
    // xu ly
    this.listBranch = [];
    let params: {};
    this.ncbService.getListBranch(params).then((result) => {
      this.listBranch.push({code: '', name: 'Tất cả'});
      result.json().body.forEach(element => {
        this.listBranch.push({
          code: element.branchCode,
          name: element.branchName,
        });
      });


    }).catch((err) => {

    });


  }
  getListPGD() {
    this.listPGD = [];
    let params: {};
    this.ncbService.getListTransaction(params).then((result) => {
      this.listPGD.push({ code: '', name: 'Tất cả' });

      result.json().body.forEach(element => {
        this.listPGD.push({
          code: element.transactionCode,
          name: element.transactionName,
        });
      });

    }).catch((err) => {

    });
  }

  getListRole() {
    this.listRole = [];
    this.ncbService.getListRole().then((result) => {
      this.listRole.push({ code: 0, name: 'Tất cả' });

      result.json().body.forEach(element => {
        this.listRole.push({
          code: element.roleId,
          name: element.roleName,
        });
      });

    }).catch((err) => {

    });
  }


}
