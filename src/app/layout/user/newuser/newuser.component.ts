import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.scss']
})
export class NewUserComponent implements OnInit {

 
  // chinnhanh: any;
  // phonggiaodich: any;
  // manhanvien: string = "";
  // tendangnhap: string = "";
  // hovaten: string = "";
  // matkhau: string = "";
  // nhaplaimatkhau: string = "";
  // sodienthoai: string = "";
  // email: string = "";
  // phanquyen: string = "";
  // superadmin: boolean = false;


  constructor(
    private router: Router,
    private httpClient:HttpClient,
  ) {
  }


  ngOnInit() {
  }

  save() {
    // let data = {
    //   chinnhanh: this.chinnhanh,
    //   phonggiaodich: this.phonggiaodich,
    //   manhanvien: this.manhanvien,
    //   tendangnhap: this.tendangnhap,
    //   hovaten: this.hovaten,
    //   matkhau: this.matkhau,
    //   nhaplaimatkhau: this.nhaplaimatkhau,
    //   sodienthoai: this.sodienthoai,
    //   email: this.email,
    //   phanquyen: this.phanquyen,
    //   superadmin: this.superadmin,
    // }
    // localStorage.setItem('__us', JSON.stringify(data));
    this.router.navigate(['/user']);
  }


}
