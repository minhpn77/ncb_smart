import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NCBService } from '../../../services/ncb.service';
import { Helper } from '../../../helper';
import { Router } from '@angular/router';


@Component({
  selector: 'edit-user',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [NCBService, Helper]
})

export class EditComponent implements OnInit {
  userForm: FormGroup;
  submitted = false;
  listPGD: any = [];
  listBranch: any = [];
  listRole: any = [];
  user: any = {
    userCode: 'Ma nhan vien',
    userName : 'Ten nhan vien',

  };

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private ncbService: NCBService,
    private helper: Helper,
    public router: Router,
  ) {}

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      branchCode: ['', Validators.required],
      posCode: ['', Validators.required],
      fullName: ['', Validators.required],
      password: ['', Validators.required],
      re_password: ['', Validators.required],
      email: ['', Validators.required],
      roleId: ['', Validators.required],
      phone: ['']
    });

    this.getListBranch();
    this.getListPGD();
    this.getListRole();

  }
  get Form() { return this.userForm.controls; }

  getListBranch() {
    // xu ly
    this.listBranch = [];
    const params = {};
    this.ncbService.getListBranch(params).then((result) => {
      this.listBranch.push({code: '', name: 'Tất cả'});
      result.json().body.forEach(element => {
        this.listBranch.push({
          code: element.branchCode,
          name: element.branchName,
        });
      });


    }).catch((err) => {

    });


  }
  getListPGD() {
    this.listPGD = [];
    const params = {};
    this.ncbService.getListTransaction(params).then((result) => {
      this.listPGD.push({ code: '', name: 'Tất cả' });

      result.json().body.forEach(element => {
        this.listPGD.push({
          code: element.transactionCode,
          name: element.transactionName,
        });
      });

    }).catch((err) => {

    });
  }
  getListRole() {
    this.listRole = [];
    this.ncbService.getListRole().then((result) => {
      this.listRole.push({ code: 0, name: 'Tất cả' });
      result.json().body.forEach(element => {
        this.listRole.push({
          code: element.roleId,
          name: element.roleName,
        });
      });

    }).catch((err) => {

    });
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.userForm.invalid) {
        return;
    }

    const formData = this.helper.urlEncodeParams(this.userForm.value);
    this.ncbService.createUser(formData).then((result) => {
      this.toastr.success('Thêm mới thành công', 'Thành công!');
      setTimeout(() => {
        this.router.navigateByUrl('/user');
      }, 500);

    }).catch((err) => {


    });
  }
  resetForm() {
    this.router.navigateByUrl('/user');
  }

}



