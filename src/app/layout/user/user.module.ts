import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewUserComponent } from './newuser/newuser.component';
import { ManageUserComponent } from './manageuser/manageuser.component';
import { EditComponent } from './edit/edit.component';
import { TranslateModule } from '@ngx-translate/core';
import { UserManagerRoutingModule } from './user.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListComponent } from './list/list.component';
import {ProfileComponent} from './profile/profile.component';

import { PageHeaderModule } from '../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        CommonModule, UserManagerRoutingModule, PageHeaderModule, NgbModule,
        TranslateModule.forChild(),
        FormsModule, ReactiveFormsModule,
        ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})
    ],
    declarations: [
        NewUserComponent, ManageUserComponent, ListComponent, EditComponent,
        ProfileComponent
    ]
})
export class UserManagerModule {}
