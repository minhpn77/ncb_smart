import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewUserComponent } from './newuser/newuser.component';
import { ManageUserComponent } from './manageuser/manageuser.component';
import { ListComponent } from './list/list.component';
import { EditComponent} from './edit/edit.component';
import {ProfileComponent} from './profile/profile.component';




const routes: Routes = [
  {
    path: '',
    children: [
    {
        path: '',
        component: ListComponent,
        data: {
          icon: 'icon-tag',
          title: 'SideBar.ProductInventory',
          // urls: [{title: 'SideBar.ProductInventory', url: '/province'}, {title: 'SideBar.ProductInventory'}]
        }
    },
    {
      path: 'create',
      component: ManageUserComponent,
      data: {
        icon: 'icon-tag',
        title: 'SideBar.ProductInventory',
        // urls: [{title: 'SideBar.ProductInventory', url: '/province'}, {title: 'SideBar.ProductInventory'}]
      }
    },
    {
      path: 'edit/:Id',
      component: EditComponent,
      data: {
        icon: 'icon-plus',
        title: 'PROV2_btt_add',
        // urls: [{title: 'SideBar.Product', url: '/province'}, {title: 'PROV2_btt_add'}]
      }
    },
    {
      path: 'profile',
      component: ProfileComponent,
      data: {
        icon: 'icon-plus',
        title: 'PROV2_btt_add',
        // urls: [{title: 'SideBar.Product', url: '/province'}, {title: 'PROV2_btt_add'}]
      }
    },
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserManagerRoutingModule {}

