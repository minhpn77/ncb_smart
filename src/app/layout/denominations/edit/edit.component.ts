import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';


@Component({
  selector: 'denomination-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  denominationId: any;

    constructor(
        public router: Router,
        private route: ActivatedRoute,
    ) {
        this.route.params.subscribe(params => {
            this.denominationId = parseInt(params.denominationId);
        });
    }

  ngOnInit() {
    console.log('--denominationId', this.denominationId);

  }


}
