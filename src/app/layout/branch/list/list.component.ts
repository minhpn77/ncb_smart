import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { NCBService } from '../../../services/ncb.service';
import { NgbModal, NgbModalRef, NgbDateStruct, NgbDatepickerConfig, NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'branch-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [NCBService]
})
export class ListComponent implements OnInit {
  private modalOp: NgbModalRef;

  search: any = {
    keyword: '',
    district: 0,
    province: 0,
    branch_name: '',
    branch_code: 0,
    page: 1,
    page_size: 10,
    total_items: 0,
    previous_page: 0,
    process_load: 0,
    process_export: 0,
    active: 2
  };
  listProvice: any = [
    {
      name: 'Tất cả',
      code: 0,
    }
  ];
  listDistrict: any = [
    {
      name: 'Vui lòng chọn quận/huyện',
      code: 0,
    }
  ];
  listBranch: any = [];
  listPageSize: any = [10, 20, 30, 40, 50];
  listStatus: any = [
    {
      name: 'Tất cả',
      code: 2,
    },
    {
      name: 'Active',
      code: 1,
    },
    {
      name: 'Inactive',
      code: 0,
    }
  ];

  constructor(
    private ncbService: NCBService,
    private modalService: NgbModal

  ) { }

  ngOnInit() {
    this.getListBranch(this.search);

  }


  getListBranch(params) {
    this.search.process_load = 1;
    // xu ly
    this.listBranch = [];
    this.ncbService.getListBranch(params).then((result) => {
      setTimeout(() => {
        this.listBranch = result.json().body;
        this.search.total_items = this.listBranch.length;
        this.search.process_load = 0;
      }, 300);

    }).catch((err) => {

    });


  }

  deleteProvince(event, index) {
    Swal.fire({
      title: 'Bạn có chắc chắn xoá?',
      text: 'Dữ liệu đã xoá không thể khôi phục lại',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Không, trở lại'
    }).then((result) => {
      if (result.value) {
        this.listBranch.splice(index, 1);
        this.search.process_load = 1;

        Swal.fire(
          'Đã xoá!',
          'Dữ liệu đã xoá hoàn toàn.',
          'success'
        );
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Huỷ bỏ',
          'Dữ liệu được bảo toàn :)',
          'error'
        );
      }
    });
  }
  cancelAction(event) {
    console.log('huy bo thanh con');

  }
  loadPage(page: number) {
    if (page !== this.search.previous_page) {
        this.search.previous_page = page;
        // this.getProduct(this.search);
    }
  }
  onSearch() {
    console.log('search', this.search);
  }
  keyDownFunction(event) {
    if (event.keyCode === 13) {
      console.log('this.search---', this.search.keyword);
        // this.getListProvince(this.search);
    }
  }
  // upload
}
