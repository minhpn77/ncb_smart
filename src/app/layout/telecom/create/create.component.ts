import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'telecom-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  telecomForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.telecomForm = this.formBuilder.group({
      name_telecom: ['', Validators.required],
      code_telecom: ['', Validators.required]

    });
  }
  get Form() { return this.telecomForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.telecomForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.telecomForm.value));
  }
  resetForm() {
    this.telecomForm.reset();
  }
}


