import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'video-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  promotionForm: FormGroup;
  submitted = false;
  Id: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute
  ) {
      this.route.params.subscribe(params => {
        this.Id = parseInt(params.Id);
    });
   }

  ngOnInit() {
    this.promotionForm = this.formBuilder.group({
      nameVNPromotion: ['', Validators.required],
      nameENPromotion: ['', Validators.required]

    });
  }
  get Form() { return this.promotionForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.promotionForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.provinceForm.value));
  }
  resetForm() {
    this.promotionForm.reset();
  }
}







