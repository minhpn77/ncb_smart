import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'configure-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  configureForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.configureForm = this.formBuilder.group({
      name_configure: ['', Validators.required],
      value_configure: ['', Validators.required]

    });
  }
  get Form() { return this.configureForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.configureForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.configureForm.value));
  }
  resetForm() {
    this.configureForm.reset();
  }
}


