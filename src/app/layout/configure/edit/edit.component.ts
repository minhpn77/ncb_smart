import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'configure-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  configureForm: FormGroup;
  submitted = false;
  Id: any;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      this.Id = parseInt(params.Id);
    });
  }

  ngOnInit() {
    this.configureForm = this.formBuilder.group({
      name_configure: ['', Validators.required],
      value_configure: ['', Validators.required]

    });
  }
  get Form() { return this.configureForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.configureForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.configureForm.value));
  }
  resetForm() {
    this.configureForm.reset();
  }
}








