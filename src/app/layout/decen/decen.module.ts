import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewDecenComponent } from './newdecen/newdecen.component';
import { ManageDecenComponent } from './managedecen/managedecen.component';
import { TranslateModule } from '@ngx-translate/core';
import { DecenRoutingModule } from './decen.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PageHeaderModule } from '../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        CommonModule, DecenRoutingModule, PageHeaderModule, NgbModule,
        TranslateModule.forChild(), FormsModule, ReactiveFormsModule
    ],
    declarations: [
        NewDecenComponent, ManageDecenComponent
    ]
})
export class DecenModule {}
