import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewDecenComponent } from './newdecen/newdecen.component';
import { ManageDecenComponent } from './managedecen/managedecen.component';


const routes: Routes = [
  {
    path: '',
    children: [
    {
      path: '',
      component: ManageDecenComponent,
      data: {
        icon: 'icon-tag',
        title: 'SideBar.ProductInventory',
        // urls: [{title: 'SideBar.ProductInventory', url: '/province'}, {title: 'SideBar.ProductInventory'}]
      }
    },
    {
      path: 'create',
      component: NewDecenComponent,
      data: {
        icon: 'icon-plus',
        title: 'PROV2_btt_add',
        // urls: [{title: 'SideBar.Product', url: '/province'}, {title: 'PROV2_btt_add'}]
      }
    },
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DecenRoutingModule {}

