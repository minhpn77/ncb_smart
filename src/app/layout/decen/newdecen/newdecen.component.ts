import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'role-create',
  templateUrl: './newdecen.component.html',
  styleUrls: ['./newdecen.component.scss']
})
export class NewDecenComponent implements OnInit {
  roleForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() {
    this.roleForm = this.formBuilder.group({
      roleName: ['', Validators.required],
      roleDescription: ['']

    });
  }
  get Form() { return this.roleForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.roleForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.roleForm.value));
  }
  resetForm() {
    this.roleForm.reset();
  }
  cancelAction() {
    this.router.navigateByUrl('/decen');
  }
}





