import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';


@Component({
  selector: 'sale-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  saleId: any;

    constructor(
        public router: Router,
        private route: ActivatedRoute,
    ) {
        this.route.params.subscribe(params => {
            this.saleId = parseInt(params.saleId);
        });
    }

  ngOnInit() {
    console.log('--provinceId', this.saleId);

  }


}
