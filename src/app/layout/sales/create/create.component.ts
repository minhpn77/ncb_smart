import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'sale-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  saleForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.saleForm = this.formBuilder.group({
      provinceCode: ['', Validators.required],
      provinceName: ['', Validators.required]
    });
  }
  get Form() { return this.saleForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.saleForm.invalid) {
        return;
    }
    this.toastr.success('Thêm mới thành công', 'Thành công!');
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.provinceForm.value));
  }
  resetForm() {
    this.saleForm.reset();
  }
}


