import { Injectable } from '@angular/core';
import { AppSettings} from '../app.settings';
import { AuthService } from '../services/auth.service';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class NCBService {

    constructor(
        private auth: AuthService
    ) { }

    getListBranch(params): Promise<any> {
        const url =  `${AppSettings.API_NCB_SMART}/branch/getAll`;
        return this.auth.authRequest({ url: url, params: params, method: 'GET'});
    }
    getListTransaction(params): Promise<any> {
        const url =  `${AppSettings.API_NCB_SMART}/transaction/getAll`;
        return this.auth.authRequest({ url: url, params: params, method: 'GET'});
    }
    getListRole(): Promise<any> {
        const url =  `${AppSettings.API_NCB_SMART}/role/get-roles`;
        return this.auth.authRequest({ url: url, method: 'GET'});
    }
    searchUser(params): Promise<any> {
        const url =  `${AppSettings.API_NCB_SMART}/user/searchUser`;
        return this.auth.authRequest({ url: url, params: params, method: 'GET'});
    }
    getListUser(params): Promise<any> {
        const url =  `${AppSettings.API_NCB_SMART}/user/get-listUser`;
        return this.auth.authRequest({ url: url, params: params, method: 'GET'});
    }
    createUser(data): Promise<any> {
        const url =  `${AppSettings.API_NCB_SMART}/user/createUser`;
        return this.auth.authRequest({ url: url, data: data, method: 'POST'});
    }

}
