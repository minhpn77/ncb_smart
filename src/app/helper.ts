import { Injectable } from '@angular/core';
import { AppComponent } from './app.component';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { isNumber, toInteger, padNumber } from '@ng-bootstrap/ng-bootstrap/util/util';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class Helper {

    public patternEmail = '^(([^<>()\[\]\\._,;:\s@"]+([\._][^<>()\[\]\\._,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    public country = [];

    constructor(
        private appComponent: AppComponent,
        // public toastr: ToastsManager,
    ) { }

    getParameterByName(name, url = '') {
        if (url === '') { url = window.location.href; }
        name = name.replace(/[\[\]]/g, '\\$&');
        const regex = new RegExp('[?&]' + name + '(=([^&]*)|&|$)'),
            results = regex.exec(url);
        if (!results) { return null; }
        if (!results[2]) { return ''; }
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    urlEncodeParams(data) {
        return Object.entries(data).map(e => e.join('=')).join('&');
    }
}
